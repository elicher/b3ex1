import logo from './logo.svg';
import './App.css';
import React from 'react';

class App extends React.Component {
  state = { data: 'No data provided!' }

  handleChange = (event) => {
    const data = event.target.value;
    this.setState({ data });
  }

  render() {
    return (
      <div className="App">
        <input name="data" onChange={this.handleChange} value={this.state.data} />

        <h1>
          You typed:
          {this.state.data}
        </h1>
      </div>
    );
  }
}

export default App;
